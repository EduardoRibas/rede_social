/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.repository;

import br.com.crescer.social.entity.Amizade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Eduardo
 */
public interface AmizadeRepository extends PagingAndSortingRepository<Amizade, Long> {

    @Override
    public Page<Amizade> findAll(Pageable pgbl);

    @Override
    public Iterable<Amizade> findAll();

    /* @Query("select u from USUARIO u where u.nome = eduardo")
      Usuario findByNome(String nome);  */
//select u from User u where u.emailAddress = ?1 and u.lastname = ?2
    @Query("select amigo from Amizade u where u.usuario = ?1")
    Iterable<Long> obterAmigosOuPendentes(Long idAtual);

    @Query("select usuario from Amizade u where u.amigo = ?1")
    Iterable<Long> obterUsuariosOuPendentes(Long idAtual);
//---------------------------------------------------------------------------------------//
    @Query("select usuario from Amizade u where u.amigo = ?1 and u.ehAmigo = ?2")
    Iterable<Long> obterPendentesOuAmigos(Long idAtual, boolean opcao);
    
    @Query("select amigo from Amizade u where u.usuario = ?1 and u.ehAmigo = ?2")
    Iterable<Long> obterPendentesOuUsuarios(Long idAtual, boolean opcao); 
//---------------------------------------------------------------------------------------//
    @Query("select a from Amizade a where a.usuario = ?1 and a.amigo = ?2")
    Amizade obterAmizade(Long idAtual, Long idAmigo);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Amizade a set a.ehAmigo = ?1 where a.id = ?2")
    int adicionarAmigo(boolean opcao, Long id);

    @Modifying
    @Transactional
    @Query("delete from Amizade u where u.id = ?1")
    void deletarAmizade(Long id);
}
