/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.repository;

import br.com.crescer.social.entity.Postagem;
import br.com.crescer.social.entity.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author eduardo.ribas
 */
public interface PostagemRepository extends PagingAndSortingRepository<Postagem, Long>{
    @Query("select p from Postagem p where p.usuario = ?1")
    Iterable<Postagem> obterPostagens(Usuario usuario);
}
