/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Eduardo
 */
@Entity
@Table(name = "AMIZADE")
public class Amizade implements Serializable {

    private static final String SQ_NAME = "SQ_AMIZADE";
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = SQ_NAME)
    @SequenceGenerator(name = SQ_NAME, sequenceName = SQ_NAME, allocationSize = 1)
    @Column(name = "ID_AMIZADE")
    private Long id;

    @Basic(optional = false)
    @Column(name = "ID_USUARIO_LOGADO")
    private Long usuario;

    @Basic(optional = false)
    @Column(name = "ID_AMIGO")
    private Long amigo;

    @Basic(optional = false)
    @Column(name = "EH_AMIGO")
    private boolean ehAmigo;

    public Long getUsuario() {
        return usuario;
    }

    public void setUsuario(Long usuario) {
        this.usuario = usuario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isEhAmigo() {
        return ehAmigo;
    }

    public void setEhAmigo(boolean ehAmigo) {
        this.ehAmigo = ehAmigo;
    }

    public Long getAmigo() {
        return amigo;
    }

    public void setAmigo(Long amigo) {
        this.amigo = amigo;
    }

}
