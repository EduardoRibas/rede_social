/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.entity;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author eduardo.ribas
 */
@Entity
@Table(name = "POSTAGEM")
public class Postagem implements Serializable, Comparable<Postagem> {

    private static final String SQ_NAME = "SQ_POSTAGEM";
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = SQ_NAME)
    @SequenceGenerator(name = SQ_NAME, sequenceName = SQ_NAME, allocationSize = 1)
    @Column(name = "ID_POSTAGEM")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ID_USUARIO")    
    private Usuario usuario;

    @Basic(optional = false)
    @Column(name = "TEXTO")
    private String texto;
    
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(value = TemporalType.DATE)
    @Basic(optional = false)
    @Column(name = "DATA_POSTAGEM")
    private Date dataPostagem;
    
    @Override
    public int compareTo(Postagem postagem) {

		int compareQuantity = ((Postagem) (postagem)).getId().intValue();

		//ascending order
		//return this.id - compareQuantity;

		//descending order
		return compareQuantity - this.id.intValue();

	}
    
    public Date getDataPostagem() {
        return dataPostagem;
    }

    public void setDataPostagem(Date dataPostagem) {
        this.dataPostagem = dataPostagem;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
