/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.repository;

import br.com.crescer.social.entity.Usuario;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Eduardo
 */
public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Long>{

    @Override
    public Page<Usuario> findAll(Pageable pgbl);
    
    public Usuario findByEmail(String username);
    
    public Usuario findByNome(String nome);
            
    List<Usuario> findByNomeContainingIgnoreCase(String nome);
    
    @Override
    public Usuario findOne(Long id);
}
