/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.servico;

import br.com.crescer.social.entity.Usuario;
import br.com.crescer.social.repository.UsuarioRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Eduardo
 */
@Service
public class UsuarioServico {
    
    @Autowired
    UsuarioRepository repository;
    
    public Page<Usuario> findAll(Pageable pgbl) {
        return repository.findAll(pgbl);
    }

    public Iterable<Usuario> findAll() {
        return repository.findAll();
    }
    
    public List<Usuario> findByNomeContainingIgnoreCase(String nome){
        return repository.findByNomeContainingIgnoreCase(nome);
    }


    public Usuario save(Usuario u) {
        return repository.save(u);
    }

    public Usuario findOne(Long id) {
        return repository.findOne(id);
    }
    
    public Usuario findByEmail(String username){
        return repository.findByEmail(username);
    }
    
    public Usuario findByNome(String nome){
        return repository.findByNome(nome);
    }

}
