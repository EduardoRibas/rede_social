/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.servico;

import br.com.crescer.social.entity.Amizade;
import br.com.crescer.social.entity.Usuario;
import br.com.crescer.social.repository.AmizadeRepository;
import br.com.crescer.social.repository.UsuarioRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author Eduardo
 */
@Service
public class AmizadeServico {

    @Autowired
    AmizadeRepository repository;

    @Autowired
    UsuarioRepository usuarioRepository;

    public Amizade save(Amizade amizade) {
        return repository.save(amizade);
    }

    public Page<Amizade> findAll(Pageable pgbl) {
        return repository.findAll(pgbl);
    }

    public Iterable<Amizade> findAll() {
        return repository.findAll();
    }

    public Amizade obterAmizade(Long idAtual, Long idAmigo) {
        return repository.obterAmizade(idAtual, idAmigo);
    }

    public Iterable<Long> obterPendentesOuAmigos(Long id, boolean opcao) {
        return repository.obterPendentesOuAmigos(id, opcao);
    }
    
    public Iterable<Long> obterPendentesOuUsuarios(Long idAtual, boolean opcao){
        return repository.obterPendentesOuUsuarios(idAtual, opcao);
    }
    
    public ArrayList<Usuario> listarAmigos(Long id){
        ArrayList<Usuario> amigos = findById(obterPendentesOuAmigos(id, true));
        amigos.addAll(findById(obterPendentesOuUsuarios(id, true)));
        return amigos;
    }
//-----------------------------------------------------------------//
    public Iterable<Long> obterUsuariosOuPendentes(Long id) {
        return repository.obterUsuariosOuPendentes(id);
    }
   
    public Iterable<Long> obterAmigosOuPendentes(Long id) {
        return repository.obterAmigosOuPendentes(id);
    }
    
    public void deletarAmizade(Long id){
        repository.deletarAmizade(id);
    }

    public ArrayList<Usuario> findById(Iterable<Long> longs) {
        ArrayList<Usuario> usuarios = new ArrayList<>();
        for (Long l : longs) {
            usuarios.add(usuarioRepository.findOne(l));
        }
        return usuarios;
    }

    public int adicionarAmigo(Long id) {
        return repository.adicionarAmigo(true, id);
    }
    // @Query("select u from USUARIO u where u.nome = eduardo")
    /*  public Usuario findByNome(String nome) {
        return repository.findByNome(nome);
    }*/
}
