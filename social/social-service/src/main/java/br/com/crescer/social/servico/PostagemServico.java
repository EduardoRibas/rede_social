/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.servico;

import br.com.crescer.social.entity.Postagem;
import br.com.crescer.social.entity.Usuario;
import br.com.crescer.social.repository.PostagemRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author eduardo.ribas
 */
@Service
public class PostagemServico {

    @Autowired
    PostagemRepository repository;
    
    @Autowired
    AmizadeServico servico;
    
    public Postagem save(Postagem p) {
        return repository.save(p);
    }

    public Page<Postagem> findAll(Pageable pgbl) {
        return repository.findAll(pgbl);
    }

    public Iterable<Postagem> findAll() {
        return repository.findAll();
    }

    public Iterable<Postagem> obterPostagens(Usuario usuario) {
        return repository.obterPostagens(usuario);
    }
    
    public ArrayList<Postagem> obterPostagemDosAmigos(ArrayList<Usuario> amigos){      
        ArrayList<Postagem> postagensDosAmigos = new ArrayList<>();
        for(Usuario u: amigos){
            Iterable<Postagem> postagens = obterPostagens(u);
            for(Postagem p : postagens){
                postagensDosAmigos.add(p);
            }
        }
        
        return postagensDosAmigos;
    }
}
