package br.com.crescer.social.security.service;

import br.com.crescer.social.entity.Usuario;
import br.com.crescer.social.security.enumeration.SocialRoles;
import br.com.crescer.social.servico.UsuarioServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Carlos H. Nonnemacher
 */
@Service
public class SocialUserDetailsService implements UserDetailsService {

    @Autowired
    UsuarioServico servico;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username.isEmpty()) {
            throw new UsernameNotFoundException(String.format("User with username=%s was not found", username));
        }
        Usuario user = servico.findByEmail(username);
        if (user != null) {
            return new User(username, user.getSenha(), SocialRoles.valuesToList());
        }
        throw new UsernameNotFoundException(String.format("User with username=%s was not found", username));
    }

}
