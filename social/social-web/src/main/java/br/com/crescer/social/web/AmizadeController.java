/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.web;

import br.com.crescer.social.entity.Amizade;
import br.com.crescer.social.entity.Usuario;
import br.com.crescer.social.servico.AmizadeServico;
import br.com.crescer.social.servico.UsuarioServico;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Eduardo
 */
@Controller
public class AmizadeController {

    @Autowired
    AmizadeServico amizadeServico;

    @Autowired
    UsuarioServico usuario;

    @RequestMapping("/amigos")
    String amigos(Model model) {
        Usuario user = obterUsuarioLogado();
        ArrayList<Usuario> pendentes = amizadeServico.findById(amizadeServico.obterPendentesOuAmigos(user.getId(), false));
        model.addAttribute("usuarios", pendentes);
        ArrayList<Usuario> amigos = amizadeServico.findById(amizadeServico.obterPendentesOuAmigos(user.getId(), true));
        amigos.addAll(amizadeServico.findById(amizadeServico.obterPendentesOuUsuarios(user.getId(), true)));
        model.addAttribute("listaAmigos", amigos);
        return "amigos";
    }

    @RequestMapping("/aceitar/{id}")
    String aceitar(@PathVariable("id") Long amigoId) {
        Amizade amizade = amizadeServico.obterAmizade(amigoId, obterUsuarioLogado().getId());
        amizadeServico.adicionarAmigo(amizade.getId());
        return "redirect:amigos";
    }

    @RequestMapping("/recusar/{id}")
    String recusar(@PathVariable("id") Long amigoId) {
        Amizade amizade = amizadeServico.obterAmizade(amigoId, obterUsuarioLogado().getId());
        amizadeServico.deletarAmizade(amizade.getId());
        return "redirect:amigos";
    }

    @RequestMapping("recusar/amigos")
    private String intermediarioRecusar(Model model) {
        return amigos(model);
    }

    @RequestMapping("aceitar/amigos")
    private String intermediarioAceitar(Model model) {
        return amigos(model);
    }

    @RequestMapping("/pesquisar")
    String listar(@RequestParam(required = false) String filtro, Model model) {
        Iterable<Usuario> usuarios = usuario.findAll();
        ArrayList<Usuario> usuariosList = toList(usuarios);
        Usuario u = obterUsuarioLogado();
        usuariosList.remove(u);
        usuariosList.removeAll(obterAmigoOuPendente());
        if (filtro == null || filtro.isEmpty()) {
            model.addAttribute("usuarios", usuariosList);
        } else {
            model.addAttribute("usuarios", filtrar(usuariosList, (ArrayList) usuario.findByNomeContainingIgnoreCase(filtro)));
        }
        return "pesquisar";
    }

    @RequestMapping("/adicionar/{id}")
    String String(@PathVariable("id") Long id) {
        Usuario usuarioLogado = obterUsuarioLogado();
        Amizade amizade = new Amizade();
        amizade.setUsuario(usuarioLogado.getId());
        amizade.setAmigo(id);
        amizade.setEhAmigo(false);
        amizadeServico.save(amizade);
        return "redirect:pesquisar";
    }

    @RequestMapping("adicionar/pesquisar")
    private String intermediario(Model model) {
        return listar(null, model);
    }

    @RequestMapping("adicionar/logout")
    String logout(HttpSession httpSession) {
        httpSession.invalidate();
        return "redirect:login";
    }

    @RequestMapping("aceitar/logout")
    String logoutAce(HttpSession httpSession) {
        httpSession.invalidate();
        return "redirect:login";
    }

    @RequestMapping("recusar/logout")
    String logoutRecusar(HttpSession httpSession) {
        httpSession.invalidate();
        return "redirect:login";
    }

    @RequestMapping("/filtrar")
    String filtrar(Usuario user, Model model, BindingResult bindingResults) {
        model.addAttribute("usuarios", usuario.findByNomeContainingIgnoreCase(user.getNome()));
        return "pesquisar";
    }

    private Set<Usuario> filtrar(ArrayList<Usuario> usuariosAindaNaoAmigos, ArrayList<Usuario> filtrados) {
        Set<Usuario> intersecao = new HashSet(usuariosAindaNaoAmigos); // pega o equals da classe
        intersecao.retainAll(filtrados);
        return intersecao;
    }

    private ArrayList<Usuario> obterAmigoOuPendente() {
        Usuario user = obterUsuarioLogado();
        ArrayList<Usuario> usuarios = amizadeServico.findById(amizadeServico.obterAmigosOuPendentes(user.getId()));
        usuarios.addAll(amizadeServico.findById(amizadeServico.obterUsuariosOuPendentes(user.getId())));
        return usuarios;
    }

    private Usuario obterUsuarioLogado() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return usuario.findByEmail(user.getUsername());
    }

    private ArrayList<Usuario> toList(Iterable<Usuario> iterable) {
        ArrayList<Usuario> list = new ArrayList<>();
        if (iterable != null) {
            for (Usuario u : iterable) {
                list.add(u);
            }
        }
        return list;
    }
}
