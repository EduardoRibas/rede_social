package br.com.crescer.social.web;

import br.com.crescer.social.entity.Usuario;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import br.com.crescer.social.servico.UsuarioServico;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author Carlos H. Nonnemacher
 */
@Controller
public class AcessController {

    @Autowired
    UsuarioServico servico;

    @RequestMapping("/login")
    String login(Usuario usuario) {
        //  model.addAttribute("usuario", new Usuario());
        return "login";
    }

    @RequestMapping("/logout")
    String logout(HttpSession httpSession) {
        httpSession.invalidate();
        return "redirect:login";
    }

    @RequestMapping(value = "/add", method = POST)
    String add(@Valid Usuario usuario, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (!bindingResult.hasErrors()) {
            if (servico.findByEmail(usuario.getEmail()) == null) {
                usuario.setSenha(new BCryptPasswordEncoder().encode(usuario.getSenha()));
                servico.save(usuario);
                return "redirect:login";
            }
            else{
                redirectAttributes.addFlashAttribute("msg", "Já existe um usuário com este email!");
            }
        }
        return "login";
    }
}
