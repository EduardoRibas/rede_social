/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.crescer.social.web;

import br.com.crescer.social.entity.Postagem;
import br.com.crescer.social.entity.Usuario;
import br.com.crescer.social.servico.AmizadeServico;
import br.com.crescer.social.servico.PostagemServico;
import br.com.crescer.social.servico.UsuarioServico;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.security.core.userdetails.User;

/**
 *
 * @author eduardo.ribas
 */
@Controller
public class FeedController {

    @Autowired
    PostagemServico servico;

    @Autowired
    UsuarioServico usuarioServico;

    @Autowired
    AmizadeServico amizadeServico;

    // private ArrayList<Postagem> listaPostagens = toList(servico.findAll());
    @RequestMapping("/feed")
    String feed(Model model) {
        Postagem postagem = new Postagem();
        model.addAttribute("postagem", postagem);
        Usuario usuario = new Usuario();
        model.addAttribute("usuario", usuario);
        ArrayList<Postagem> postagensAmigos = servico.obterPostagemDosAmigos(amizadeServico.listarAmigos(obterUsuarioLogado().getId()));
        postagensAmigos.addAll(iterableToList(servico.obterPostagens(obterUsuarioLogado())));
        model.addAttribute("postagens", toArray(postagensAmigos));
        Usuario user = obterUsuarioLogado();
        model.addAttribute("atual", user);
        ArrayList<Usuario> amigos = amizadeServico.findById(amizadeServico.obterPendentesOuAmigos(user.getId(), true));
        amigos.addAll(amizadeServico.findById(amizadeServico.obterPendentesOuUsuarios(user.getId(), true)));
        model.addAttribute("usuarios", amigos);
        return "feed";
    }

    @RequestMapping(value = "/postar", method = POST)
    String postar(@Valid Postagem postagem, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        /* if (!bindingResult.hasErrors()) {
        }*/
        postagem.setUsuario(obterUsuarioLogado());
        //  postagem.setUsuario(usuarioServico.findByEmail(((User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail()));
        postagem.setDataPostagem(new Date());
        servico.save(postagem);
        return "redirect:feed";
        // return "feed";
    }

    private Usuario obterUsuarioLogado() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return usuarioServico.findByEmail(user.getUsername());
    }

    private ArrayList<Postagem> iterableToList(Iterable<Postagem> iterable) {
        ArrayList<Postagem> postagens = new ArrayList<>();
        for (Postagem p : iterable) {
            postagens.add(p);
        }
        return postagens;
    }

    private Postagem[] toArray(ArrayList<Postagem> list) {
        int cont = list.size();
        if (cont > 0) {
            Postagem[] array = new Postagem[cont];
            int i = 0;
            for (Postagem p : list) {
                array[i++] = p;
            }
            Arrays.sort(array);

            return array;
        } else {
            return null;
        }
    }
}
